import React, { useState } from 'react';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';
import MenuElement from "./Menu";

const App: React.FC = () => (
  
    <MenuElement />

);
export default App;